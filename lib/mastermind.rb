class Code
  attr_reader :pegs
  #
  PEGS = {
    "B" => "BLUE",
    "Y" => "YELLOW",
    "O" => "ORANGE",
    "R" => "RED",
    "G" => "GREEN",
    "P" => "PURPLE"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    peg = str.split('').map do |ch|
      raise "Not a valid code" unless PEGS.key?(ch.upcase)
      PEGS[ch.upcase]
    end
    Code.new(peg)
  end

  def self.random
    pegs = []
    until pegs.size == 4
      pegs << PEGS.keys.sample
    end
    Code.new(pegs.join)
  end

  def [](int)
    pegs[int]
  end

  def exact_matches(other_code)
    exact_match = 0
    pegs.each_index do |idx|
      exact_match += 1 if pegs[idx] == other_code[idx]
    end
    exact_match
  end

  def near_matches(other_code)
    near_matches = 0
    pegs.uniq.each do |color|
      near_matches += [pegs.count(color), other_code.pegs.count(color)].sort[0]
    end
    near_matches - exact_matches(other_code)
  end

  def ==(other_code)
    return true if exact_matches(other_code) == 4
    false
  end


end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "What is your guess:"
    guess = gets.chomp
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    #
    puts "Hey man, you got #{exact_matches} exact matches"
    puts "You got #{near_matches} near matches"
    guess
  end
end
